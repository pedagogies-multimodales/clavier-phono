<?php session_start (); //pour que le reste du code utilise la session en cours
	try {
		//PDO(lien vers la BDD)
		$bdd=new PDO('mysql:host=localhost;dbname=gaubilr','gaubilr','gaubilr');

		$dico=Array();

		$requete = 'SELECT * FROM DicoRaccourcis';
		$reponse = $bdd->query($requete);
		while ($donnees = $reponse -> fetch()){
			//enlever les "\r" à la fin des transcriptions normées
			$donnees['transcription_normee'] = trim($donnees['transcription_normee'],"\r");
			
			//si la forme lue est déjà présente dans le dico
			if(array_key_exists($donnees['transcription_normee'],$dico)){
				//on concatène le résultat trouvé au(x) précédent(s)
				$dico[$donnees['transcription_normee']].=",".$donnees['forme'];
			}
			//sinon on ajoute la transcription en clef du dico et la forme en valeur
			else{
				$dico[$donnees['transcription_normee']]=$donnees['forme'];
			}			
		}

		$json = json_encode($dico);
	}
	catch (Exception $e){
		die ("Erreur :".$e->getMessage());
		print "erreur";
	}
?>
