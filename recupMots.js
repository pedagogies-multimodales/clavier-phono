//tableau associatif, SAMPA => API
var tabPhon = { 
    "rect_E~":"ɛ̃", 
    "rect_A~":"ɑ̃", 
    "rect_O~":"ɔ̃",
    "rect_i":"i",
    "rect_y":"y",
    "rect_e":"e",
    "rect_E":"ɛ",
    "rect_2":"ø",
    "rect_9":"œ",
    "rect_@":"ə",
    "rect_O":"o",
    "rect_a":"a",
    "rect_u":"u",
    "rect_o":"ɔ",
    "rect_w":"w",
    "rect_H":"ɥ",
    "rect_j":"j",
    "rect_k":"k",
    "rect_g":"ɡ",
    "rect_N":"ɲ",
    "rect_R":"ʁ",
    "rect_t":"t",
    "rect_d":"d",
    "rect_n":"n",
    "rect_l":"l",
    "rect_p":"p",
    "rect_b":"b",
    "rect_m":"m",
    "rect_f":"f",
    "rect_v":"v",
    "rect_s":"s",
    "rect_z":"z",
    "rect_S":"ʃ",
    "rect_Z":"ʒ",
    "rect_wE~":"wɛ̃",
    "rect_wa":"wa",
    "rect_Hi":"ɥi",
    "rect_ks":"ks",
    "rect_gz":"ɡz",
    "rect_nj":"ɲ",
    "rect_dZ":"dz",
  };

//dictionnaire associatif phonétique => mot (ou liste de mots)
var dico = new Object();
//variable qui contiendra le mot en phonétique à rechercher dans le dictionnaire
var motARechercher="";
//s'exécute au chargement de la page : récupération du dictionnaire
window.onload = function() {
  
  $.ajax({
    url: 'recupDico.php',
    method: 'GET',
    dataType:'json',
    success:function(data){
      // met le json dans notre dictionnaire
      dico = data;
    }
  });
}

//récupération du phonème sélectionné par l'utilisateur
function recupPhon(identifiant){
  //composition du mot en phonétique
  motARechercher+=tabPhon[identifiant];
  document.getElementById('zoneRetour').innerHTML+=tabPhon[identifiant];
}

//effacer un phonème en cas d'erreur
function effacer(identifiant){
  //composition du mot en phonétique
  motARechercher=motARechercher.substring(0,motARechercher.length-1);
  document.getElementById('zoneRetour').innerHTML=motARechercher;
}

//cherche la suite de phonèmes écrite dans le dictionnaire
function rechercher(){
  //si la suite de phonèmes existe
  if(dico[motARechercher]!=null){
    
    //s'il n'y a qu'une seule correspondance
    if(dico[motARechercher].search(",")==-1){
      document.getElementById('zoneRetour').innerHTML+=" : "+dico[motARechercher];
    }
    //s'il y a plusieurs correspondances
    else {
      var resultats = dico[motARechercher].split(",");
      document.getElementById('zoneRetour').innerHTML+=" : "+resultats[0];
      //on affiche toutes les solutions
      for(var i=1; i<resultats.length ; i++){
        document.getElementById('zoneRetour').innerHTML+=", "+resultats[i];
      }
    }
    
  }
  else {
    document.getElementById('zoneRetour').innerHTML="Le mot \""+motARechercher+"\" n'existe pas.";
  }
  //réinitialisation du mot à rechercher
  motARechercher="";
}

//effectuer une nouvelle requête
function recommencer(){
  //réinitialisation du mot à rechercher
  motARechercher="";
  document.getElementById('zoneRetour').innerHTML="";
}