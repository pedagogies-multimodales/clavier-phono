-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 10 Décembre 2019 à 17:51
-- Version du serveur :  10.1.43-MariaDB-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gaubilr`
--

-- --------------------------------------------------------

--
-- Structure de la table `DicoRaccourcis`
--

CREATE TABLE `DicoRaccourcis` (
  `id` int(10) UNSIGNED NOT NULL,
  `forme` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `transcription` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `transcription_normee` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `DicoRaccourcis`
--

INSERT INTO `DicoRaccourcis` (`id`, `forme`, `transcription`, `transcription_normee`) VALUES
(1, 'accueil', 'a.kœj', 'akœj\r'),
(2, 'lire', 'liʁ', 'liʁ\r'),
(3, 'encyclopédie', 'ɑ̃.si.klɔ.pe.di', 'ɑ̃siklɔpedi\r'),
(4, 'manga', 'mɑ̃.ɡa', 'mɑ̃ɡa\r'),
(5, 'ouvrage', 'u.vʁaʒ', 'uvʁaʒ\r'),
(6, 'siège', 'sjɛʒ', 'sjɛʒ\r'),
(7, 'chaise', 'r', 'r\r'),
(8, 'fauteuil', 'fo.tœj', 'fotœj\r'),
(9, 'meuble', 'mœbl', 'mœbl\r'),
(10, 'mardi', 'maʁ.di', 'maʁdi\r'),
(11, 'lundi', 'lœ̃.di', 'lœ̃di\r'),
(12, 'semaine', 'sə.mɛn', 'səmɛn\r'),
(13, 'militaire', 'mi.li.tɛʁ', 'militɛʁ\r'),
(14, 'suis', 'sɥi', 'sɥi\r'),
(15, 'barbe à papa', 'baʁ.b‿a pa.pa', 'baʁb‿a papa\r'),
(16, 'manchot', 'mɑ̃.ʃo', 'mɑ̃ʃo\r'),
(17, 'pingouin', 'pɛ̃.ɡwɛ̃', 'pɛ̃ɡwɛ̃\r'),
(18, 'mercredi', 'mɛʁ.kʁə.di', 'mɛʁkʁədi\r'),
(19, 'bande dessinée', 'bɑ̃d dɛ.si.ne', 'bɑ̃d dɛsine\r'),
(20, 'oiseau', 'wa.zo', 'wazo\r'),
(21, 'allemand', 'al.mɑ̃', 'almɑ̃\r'),
(22, 'Allemande', 'al.mɑ̃d', 'almɑ̃d\r'),
(23, 'Allemands', 'al.mɑ̃', 'almɑ̃\r'),
(24, 'jeudi', 'ʒø.di', 'ʒødi\r'),
(25, 'vendredi', 'vɑ̃.dʁə.di', 'vɑ̃dʁədi\r'),
(26, 'ailé', 'ɛ.le', 'ɛle\r'),
(27, 'voler', 'vɔ.le', 'vɔle\r'),
(28, 'ovipare', 'ɔ.vi.paʁ', 'ɔvipaʁ\r'),
(29, 'poisson', 'pwa.sɔ̃', 'pwasɔ̃\r'),
(30, 'travail', 'tʁa.vaj', 'tʁavaj\r'),
(31, 'armée', 'aʁ.me', 'aʁme\r'),
(32, 'nouveau', 'nu.vo', 'nuvo\r'),
(33, 'pinyin', 'pin.jin', 'pinjin\r'),
(34, 'sinogramme', 'si.nɔ.ɡʁam', 'sinɔɡʁam\r'),
(35, 'kanji', 'kan.ʒi', 'kanʒi\r'),
(36, 'chair à canon', 'ʃɛ.ʁ‿a ka.nɔ̃', 'ʃɛʁ‿a kanɔ̃\r'),
(37, 'jour', 'ʒuʁ', 'ʒuʁ\r'),
(38, 'groupe', 'ɡʁup', 'ɡʁup\r'),
(39, 'école', 'e.kɔl', 'ekɔl\r'),
(40, 'CD-ROM', 'se.de.ʁɔm', 'sedeʁɔm\r'),
(41, 'photographie', 'fɔ.to.ɡʁa.fi', 'fɔtoɡʁafi\r'),
(42, 'ordinateur', 'ɔʁ.di.na.tœʁ', 'ɔʁdinatœʁ\r'),
(43, 'janvier', 'ʒɑ̃.vje', 'ʒɑ̃vje\r'),
(44, 'année', 'a.ne', 'ane\r'),
(45, 'février', 'fe.vʁi.je', 'fevʁije\r'),
(46, 'mars', 'maʁs', 'maʁs\r'),
(47, 'avril', 'a.vʁil', 'avʁil\r'),
(48, 'mai', 'mɛ', 'mɛ\r'),
(49, 'juin', 'ʒɥɛ̃', 'ʒɥɛ̃\r'),
(50, 'juillet', 'ʒɥi.jɛ', 'ʒɥijɛ\r'),
(51, 'octobre', 'ɔk.tɔbʁ', 'ɔktɔbʁ\r'),
(52, 'novembre', 'nɔ.vɑ̃bʁ', 'nɔvɑ̃bʁ\r'),
(53, 'décembre', 'de.sɑ̃bʁ', 'desɑ̃bʁ\r'),
(54, 'computer', 'kɔ̃.py.te', 'kɔ̃pyte\r'),
(55, 'anglais', 'ɑ̃.ɡlɛ', 'ɑ̃ɡlɛ\r'),
(56, 'lieu', 'ljø', 'ljø\r'),
(57, 'physique', 'fi.zik', 'fizik\r'),
(58, 'concertation', 'kɔ̃.sɛʁ.ta.sjɔ̃', 'kɔ̃sɛʁtasjɔ̃\r'),
(59, 'interrogation', 'ɛ̃.tɛ.ʁɔ.ɡa.sjɔ̃', 'ɛ̃tɛʁɔɡasjɔ̃\r'),
(60, 'homogène', 'ɔ.mɔ.ʒɛn', 'ɔmɔʒɛn\r'),
(61, 'imprimante', 'ɛ̃.pʁi.mɑ̃t', 'ɛ̃pʁimɑ̃t\r'),
(62, 'empirique', 'ɑ̃.pi.ʁik', 'ɑ̃piʁik\r'),
(63, 'observation', 'ɔp.sɛʁ.va.sjɔ̃', 'ɔpsɛʁvasjɔ̃\r'),
(64, 'hypothèse', 'i.pɔ.tɛz', 'ipɔtɛz\r'),
(65, 'collaboration', 'kɔ.la.bɔ.ʁa.sjɔ̃', 'kɔlabɔʁasjɔ̃\r'),
(66, 'samedi', 'sam.di', 'samdi\r'),
(67, 'dimanche', 'di.mɑ̃ʃ', 'dimɑ̃ʃ\r'),
(68, 'prendre', 'pʁɑ̃dʁ', 'pʁɑ̃dʁ\r'),
(69, 'le', 'lə', 'lə\r'),
(70, 'la', 'la', 'la\r'),
(71, 'les', 'le', 'le\r'),
(72, 'fin', 'fɛ̃', 'fɛ̃\r'),
(73, 'abréviation', 'a.bʁe.vja.sjɔ̃', 'abʁevjasjɔ̃\r'),
(74, 'essai', 'e.sɛ', 'esɛ\r'),
(75, 'chinois', 'ʃi.nwa', 'ʃinwa\r'),
(76, 'procrastiner', 'pʁɔ.kʁas.ti.ne', 'pʁɔkʁastine\r'),
(77, 'espagnol', 'ɛs.pa.ɲɔl', 'ɛspaɲɔl\r'),
(78, 'neuf', 'nœf', 'nœf\r'),
(79, 'et', 'e', 'e\r'),
(80, 'un', 'œ̃', 'œ̃\r'),
(81, 'nord', 'nɔʁ', 'nɔʁ\r'),
(82, 'sud', 'syd', 'syd\r'),
(83, 'localisation', 'lɔ.ka.li.za.sjɔ̃', 'lɔkalizasjɔ̃\r'),
(84, 'terre', 'tɛʁ', 'tɛʁ\r'),
(85, 'dico', 'di.ko', 'diko\r'),
(86, 'je', 'ʒ', 'ʒ\r'),
(87, 'mot', 'mo', 'mo\r'),
(88, 'une', 'yn', 'yn\r'),
(89, 'aux', 'o', 'o\r'),
(90, 'personne', 'pɛʁ.sɔn', 'pɛʁsɔn\r'),
(91, 'green', 'ɡʁin', 'ɡʁin\r'),
(92, 'plage', 'plaʒ', 'plaʒ\r'),
(93, 'lès', 'lɛ', 'lɛ\r'),
(94, 'Wiktionnaire:Patron d’article pour un nom commun', 'pʁɔ.nɔ̃.sja.sjɔ̃', 'pʁɔnɔ̃sjasjɔ̃\r'),
(95, 'nom commun', 'nɔ̃ kɔ.mœ̃', 'nɔ̃ kɔmœ̃\r'),
(96, 'des', 'de', 'de\r'),
(97, 'ou', 'u', 'u\r'),
(98, 'cérémonie', 'se.ʁe.mɔ.ni', 'seʁemɔni\r'),
(99, 'Wiktionnaire:Patron d’article pour un nom propre', 'pʀɔ.nɔ̃.sja.sjɔ̃', 'pʀɔnɔ̃sjasjɔ̃\r'),
(100, 'Toulouse', 'tu.luz', 'tuluz\r');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
